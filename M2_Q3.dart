// Defining class
class Winner {
  var appName;
  var appCatergory;
  var appDev;
  var appYear;

  // defining class function
  showAppInfo() {
    print("Best App : ${appName.toUpperCase()}");
    print("App Catergory : ${appCatergory}");
    print("App Developer : ${appDev}");
    print("The year it won MTN Business App of the Year Awards : ${appYear}");
  }
}

void main() {
  // Creating object
  var app = new Winner();
  app.appName = "Sisa";
  app.appCatergory = "Best Hackathon Solution";
  app.appDev = "Claudia Mabuza";
  app.appYear = 2021;
// Accessing class Function
  app.showAppInfo();
}

//Rethabile Mokotjo
